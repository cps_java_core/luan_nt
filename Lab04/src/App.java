import java.util.Scanner;

public class App {
    public static Scanner sc=new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        
        int sttMenu=0;
        int soMenu=3;
        xuatMenu();
        do {
            sttMenu=chonMenu(sc, soMenu);
            xuLyMenu(sttMenu);
        } while (true);
        
    
    }
    public static void xuatMenu(){
        System.out.println("===========================Menu===========================");
        System.out.println("0. Thoát Chương Trình. ");
        System.out.println("1. Thực Hiện Phép Toán 1+1/n*3. ");
        System.out.println("2. Thực Hiện Phép Toán 1+(2n+1)/(2n+3). ");
        System.out.println("3. Nhập 3 Cạnh A, B, C Và Phân Loại Tam Giác. ");
        System.out.println("4. Diện Tích Tam Giác. ");
        System.out.println("5. Chuyển Đổi tiền Tệ. ");
        System.out.println("6. Tìm Ký Tự Trong Mảng. ");
        System.out.println("7. Chuyển đổi chuỗi sau thành một mảng: “Hello_I_am_Iron_Man!”.");
        System.out.println("==========================================================");        
    }
    public static int chonMenu(Scanner sc,int soMenu){
        soMenu=0;
        int chonMenu=0;
        
        do {
            System.out.println("Nhập Chức Năng Menu:");
            
            chonMenu=HamTinhToan.nhapSoNguyenDuong();
           
        } while (0<=chonMenu && chonMenu<=soMenu);
        return chonMenu;
    }
    public static void xuLyMenu(int sttMenu){
        switch (sttMenu) {
            case 0:
                System.out.println("0. Thoát Chương Trình");
                System.exit(0);
                break;
            case 1:
                System.out.println("1. Thực Hiện Phép Toán 1+1/n*3");
                HamTinhToan.cauMot();
                break;
            case 2:
                System.out.println("2. Thực Hiện Phép Toán 1+(2n+1)/(2n+3)");
                HamTinhToan.cauHai();
                break;
            case 3:
                System.out.println("3. Nhập 3 Cạnh A, B, C Và Phân Loại Tam Giác. ");                         
                HamTinhToan.phanLoaiTG();
                break;
            case 4:
                System.out.println("4. Diện Tích Tam Giác. ");
                HamTinhToan.tinhDienTichTG();
                break;
            case 5:
                System.out.println("5. Chuyển Đổi tiền Tệ. ");            
                HamTinhToan.chuyenTien();;
                break;
            case 6:
                System.out.println("6. Tìm Ký Tự Trong Mảng. ");
                HamTinhToan.timKyTuXuatHien();
                break; 
            case 7:
                System.out.println("7. Chuyển đổi chuỗi sau thành một mảng: “Hello_I_am_Iron_Man!”.");
                HamTinhToan.chuyenStringThanhArr();
                break;
        }
    }
     
}
