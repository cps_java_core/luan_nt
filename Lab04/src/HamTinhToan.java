import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner; 
public class HamTinhToan {
    public static Scanner sc=new Scanner(System.in);
    public static int nhapSoNguyenDuong(){
        int giaTriNhap=0;
        while(true){
            try {
                final String value = sc.nextLine();
                giaTriNhap = Integer.parseInt(value);
                if (giaTriNhap >= 0) {
                    break;
                } else {
                    System.out.println("Bạn Nhập Số Âm !!!");
                    System.out.println("Bạn Hãy Nhập Lại !!!");
                    continue;
                }
            } catch (final Exception e) {
                System.out.println("Bạn Nhập Sai !!!");
                System.out.println("Bạn Hãy Nhâp Lại !!!");
            }
            
        }
        
        return giaTriNhap;
    }

    public static double cauMot() {
        double sum = 1;
        int n = 0;
        System.out.print("Nhập Giá Trị Của n =  ");
        n = nhapSoNguyenDuong();

        for (int i = 1; i <= n; i++) {
            sum = sum + (double) 1 / (i * 3);
        }
        System.out.println("Kết Quả Phép Tính Là: " + (double) (Math.ceil((double) sum * 1000) / 1000));
        return sum;
    }

    public static double cauHai() {
        double sum = 1;
        double tich = 1;
        System.out.print("Nhập Giá Trị Của n =  ");
        final int n = nhapSoNguyenDuong();
        for (double i = 0; i <= n; i++) {
            tich*=((2*(i+1))/(2*i+3));
            sum+=tich;
        }
        System.out.println("Kết Quả Phép Tính Là: "+sum);
        return sum;
    }
    public static void nhapChieuDaiCanh(int a, int b, int c){
         a=0;b=0;c=0;

        System.out.println("Nhập Chiều Dài Cạnh A: ");
        a=nhapSoNguyenDuong();
        System.out.println("Nhập Chiều Dài Cạnh B: ");
        b=nhapSoNguyenDuong();
        System.out.println("Nhập Chiều Dài Cạnh C: ");
        c=nhapSoNguyenDuong();           
        
    }
    public static int phanLoaiTG(){
        int a = 0;
        int b = 0;
        int c = 0;
        nhapChieuDaiCanh(a, b, c);
        if (a < b + c && b < a + c && c < a + b) {
            if (a * a == b * b + c * c || b * b == a * a + c * c || c * c == a * a + b * b) {
                System.out.println("Đây Là Tam Giác Vuông");
            } else if (a == b && b == c) {
                System.out.println("Đây Là Tam Giác Đều");
            } else if (a == b || a == c || b == c) {
                System.out.println("Đây Là Tam Giác Cân");
            } else if (a * a > b * b + c * c || b * b > a * a + c * c || c * c > a * a + b * b) {
                System.out.println("Đây Là Tam Giác Tù");
            } else {
                System.out.println("Đây Là Tam Giác Nhọn");
            }
        } else {
            System.out.println("3 Cạnh A, B, C Không Phải Là Cạnh Của Tam Giác.");

        }
        return phanLoaiTG();

    }

    public static double tinhDienTichTG() {
        // công thức heron
        final int a = 0;
        final int b = 0;
        final int c = 0;
        nhapChieuDaiCanh(a, b, c);
        final double p = (float) (a + b  + c) / 2;
        double s;
        s = (double) Math.sqrt(p * (p - a) * (p - b) * (p - c));
        System.out.println("Diện tích hình tam giác là :" + s);
        return s;
    }
    

    public static void chuyenTien() {        
        int tien = 0;
        int value=0;        
        int[] tiGia={23000,26000,210,308,1};
        double KQ= 0;
        String loaiTien;
        String tienHT;
        String[] maTienTe={"USD","EUR","JPY","RUB","VND"};
        String key;
        String chonTien;
        

        HashMap<String, Integer> doiTien = new HashMap <String, Integer>();
        
        for (int i = 0; i < maTienTe.length; i++) {         
            key=maTienTe[i];
            for (int j = 0; j <=i;j++) {                
                value=tiGia[j];               
                doiTien.put(key, value);     
                
            }
            
        }  

        // cập nhật tỉ giá tiền tệ
        do {
            System.out.println("Bạn Có Muốn Cập Nhật Tỉ Giá Tiền Tệ: Y/N ");
            tienHT=sc.nextLine();
            tienHT=tienHT.toUpperCase();
            try {
                if(tienHT.equals("Y")){
                    System.out.println("Bạn Sẽ Cập Nhật Giá Trị Tỉ Giá Tiền Tệ: ");                    
                    for (int i = 0; i < maTienTe.length; i++) {
                        System.out.print(maTienTe[i]+"\t");
                        
                    }
                    System.out.println("\nBạn Hãy Nhập Tỉ Giá NgoạiTệ: ");
                    for (int i = 0; i < tiGia.length; i++) {                                            
                        tiGia[i]=nhapSoNguyenDuong();
                                                                     
                    }
                    System.out.println(tiGia); 
                    for (int i = 0; i < maTienTe.length; i++) {         
                        key=maTienTe[i];
                        for (int j = 0; j <=i;j++) {                
                            value=tiGia[j];               
                            doiTien.put(key, value);     
                            
                        }
                        
                    }
                    System.out.println("Cập Nhật Thành Công! ");
                    System.out.println(doiTien);
                    break;
                    
                }else{
                    System.out.println("Bạn Đã Chọn Không Cập Nhật Tỉ Giá Tiền Tệ.");
                    break;
                }
            } catch (Exception e) {
                //TODO: handle exception
            }
        } while (true);  
        // chọn loại tiền bạn đang có
        System.out.println("Chọn Loại Tiền");     
        System.out.println("\n"+doiTien.keySet());
        do {           
            chonTien=sc.nextLine();            
            if(chonTien.length()==3){
                try {
                    int inter=0;
                    inter=Integer.parseInt(chonTien);
                    System.out.println("Bạn Nhập Số");
                    continue;                        
                } catch (Exception e){                                            
                }      
                break;
            }else{
                System.out.println("Bạn Nhập Sai.");                
            }                 
        } while (true);
        switch (chonTien.toUpperCase()) {
            case "USD":
                System.out.println("Bạn Đã Chọn Loại Tiền USD: ");
                System.out.print("Nhập Số USD: ");
                tien=nhapSoNguyenDuong()*doiTien.get("USD");
                break;
            case "EUR":
                System.out.println("Bạn Đã Chọn Loại Tiền EUR: ");
                System.out.print("Nhập Số EUR: ");
                tien=nhapSoNguyenDuong()*doiTien.get("EUR");
                break;
            case "JPY":
                System.out.println("Bạn Đã Chọn Loại Tiền JPY: ");
                System.out.print("Nhập Số JPY: ");
                tien=nhapSoNguyenDuong()*doiTien.get("JPY");
                break;
            case "RUB":
                System.out.println("Bạn Đã Chọn Loại Tiền EUR: ");
                System.out.print("Nhập Số RUB: ");
                tien=nhapSoNguyenDuong()*doiTien.get("RUB");
            case "VND":
                System.out.println("Bạn Đã Chọn Loại Tiền VND: ");
                System.out.print("Nhập Số VND: ");
                tien=nhapSoNguyenDuong()*doiTien.get("VND");
                break;
            
            default:
                System.out.println("Loại Tiền Bạn Nhập Chưa Có!!!");
                break;
        }    
       
        System.out.println("");
        System.out.println("=> Chọn Loại Tiền Bạn Muốn Chuyển: ");
        System.out.println("Loại Tiền Bạn Muốn Chuyển: ");        
        for ( String i : doiTien.keySet()) {
            System.out.print(i+"\t");
        }     
       

        // kiểm tra điều kiện mã ==3
        System.out.println("");
        System.out.println("Bạn Nhập Mã Tiền Tệ Tương Ứng: ");
        do {           
            loaiTien=sc.nextLine();            
            if(loaiTien.length()==3){
                try {
                    int inter=0;
                    inter=Integer.parseInt(loaiTien);
                    System.out.println("Bạn Nhập Số");
                    continue;
                        
                } catch (Exception e){                 
                                           
                }      
                break;

            }else{
                System.out.println("Bạn Nhập Sai.");
                
            }       
                 
        } while (true);
        
        //loaiTien.toUpperCase() viết in hoa
        
        switch (loaiTien.toUpperCase()){
            case "USD":
                KQ=(double)tien/doiTien.get("USD");
                System.out.println("Đã Chuyển Xong = "+KQ+" $");
                break;
            case "EUR":
                KQ=(double)tien/doiTien.get("ERU");
                System.out.println("Đã Chuyển Xong = "+KQ+" €");
                break;
            case "YPJ":
                KQ=(double)tien/doiTien.get("YPJ");
                System.out.println("Đã Chuyển Xong = "+KQ+" ¥");
                break;
            case "RUB":
                KQ=(double)tien/doiTien.get("RUB");
                System.out.println("Đã Chuyển Xong = "+KQ+" ₽");
                break;
            case "VND":
                KQ=(double)tien/doiTien.get("RUB");
                System.out.println("Đã Chuyển Xong = "+KQ+" đ");
                break;
            default:
                System.out.println("Loại Tiền Bạn Nhập Chưa Có!!!");
                break;
            
        }        
    }
    public static void timKyTuXuatHien(){
        int count=0;
        System.out.println("Nhập Kích Thức Mảng: ");
        int n=nhapSoNguyenDuong();
        String str;        
        String kyTu;
        String arrString[]=new String[n];
        System.out.println("Nhập Phần Tử: ");
        for (int i = 0; i < n; i++) {
            str=sc.nextLine();
            arrString[i]=str;
            System.out.println("Phần Tử Đã Nhập: "+arrString[i]);
        }
       
        List<String> list = Arrays.asList(arrString);
        System.out.println(list);
        do {
            System.out.println("Nhập 1 Ký Tự Cần Tìm: ");
            kyTu=sc.nextLine();
            try {
                if(kyTu.length()==1){
                    break;
                }else{
                    System.out.println("Bạn Nhập Thừa Ký Tự!!!");
                    continue;
                }
            } catch (Exception e) {
                //TODO: handle exception
            }
        } while (true);
        
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).equals(kyTu)){
                count++;
            }
        }
        System.out.println("Số Lần Xuất Hiện Của Kí Tự ["+kyTu+"] LÀ: "+count);
        sc.close();
    }  
    public static String[] chuyenStringThanhArr(){        
        String str="Hello_I_am_Iron_Man!";
        str.toCharArray();
        System.out.println(str);
        return chuyenStringThanhArr();
    }
    
   
}