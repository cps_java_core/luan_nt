import java.util.Random;
import java.util.Scanner;
public class HamTinhToan {
        // câu 1 
    public static Scanner sc  = new Scanner(System.in);
    public static Random rDom = new Random();
    public static int nhapSoNguyenDuong() {
        int value = 0;        
        while (true) { 
            final String temp = sc.nextLine();
            try {
                value = Integer.parseInt(temp);
                if(value>=0){
                    break;
                }else{
                    System.out.println("Bạn nhập số âm: ");
                    continue;
                }
                
            } catch (final Exception e) {
                System.out.println("Bạn Nhập Không Phải Số !!! ");
               
            }
            
        }
         
        return value;
    }
    public static int[] nhapRandom(){
        int n = kichThuocArr();
        int[] Arr=new int [n];
        for(int i=0; i<n;i++){
            Arr[i]=rDom.nextInt(n*10);
        }
        
        return Arr;
    }
    

    //hàm nhập kích thước mảng là số dương
    public static int kichThuocArr(){
        int kich_Thuoc=0;        
        System.out.println("bạn nhập kích thước mảng: ");
        kich_Thuoc=nhapSoNguyenDuong();        
        System.out.println("kích thước mảng là: "+kich_Thuoc);
        return kich_Thuoc;   
    }
    //nhập phần tử mảng
    public static int[] inputPTArr(final int n){
        final int[] Arr = new int[n];
        for(int i=0; i < n; i++){
            System.out.printf("a[%d] = " , i);
            Arr[i]=nhapSoNguyenDuong();

        }
        return Arr;
    }
     // xuất mảng
    public static int[] xuatArr(int Arr[]){
        System.out.println("Gia tri cua mang la: ");  
        for(int i=0; i<Arr.length;i++){
            System.out.println(Arr[i]);
        }
        return Arr;
    }
    // min trong mảng
    public static int minArr(){
        final int n = kichThuocArr(); int Arr[];
        Arr=inputPTArr(n);
        int min_Arr = Arr[0];
        for(int i=0;i<Arr.length;i++){
            if(Arr[i] < min_Arr){
                min_Arr=Arr[i];
            }
        }
        System.out.println("Min của mảng là: "+ min_Arr);        
         return min_Arr;
     
   }
   // max trong mảng
    public static int maxArr(){
        final int n = kichThuocArr(); int Arr[];
        Arr=inputPTArr(n);
        int max_Arr = Arr[0];
            for(int i=0;i<Arr.length;i++){
                if(Arr[i] > max_Arr){
                max_Arr=Arr[i];
            }
    }
    System.out.println("Max của mảng là: "+ max_Arr);        
     return max_Arr; 
    }
    // từng min trong mảng
    public static int[] allMinArr(){
        final int n = kichThuocArr(); int Arr[];
        Arr=inputPTArr(n);
        int min_Arr = Arr[0];
           
            for(int i=0;i<Arr.length;i++){
                if(Arr[i] < min_Arr){
                min_Arr=Arr[i];
                System.out.println("từng min của mảng "+ min_Arr); 
            }           
        }    
        return Arr;    
    }
    // từng là max trong mảng
    public static int[] allMaxArr(){
        int Arr[];
        int n=kichThuocArr();
        Arr=inputPTArr(n);
        int max_Arr = Integer.MIN_VALUE;
            for(int i=0;i<Arr.length;i++){
                if(Arr[i] > max_Arr){
                max_Arr=Arr[i];
                System.out.println("từng max của mảng "+ max_Arr); 
            }
    }
            
     return Arr; 
    }
    // tổng số chẳn trong mảng
    public static int tinhChanArr(){        
        int Arr[];
        Arr=nhapRandom();
        int tongChan=0;
        
        for(int i=0;i<Arr.length;i++){
            if(Arr[i] %2 ==0){
                tongChan+=Arr[i];
            }        
        }
        System.out.println("Tổng các số lẻ trong mảng: "+ tongChan);
        return tongChan;
    }
    // tổng số lẻ trong mảng
    public static int tinhLeArr(){
        int[]Arr=nhapRandom();
        int tongLE=0;
        for(int i=0;i<Arr.length;i++){            
            if(Arr[i] %2 !=0){
                tongLE+=Arr[i];
               
            }                  
        }
        System.out.println("Tổng các số lẻ trong mảng: "+ tongLE);
        return tongLE;
    }
    // tìm số lẻ
    public static int[] timSoLe(){
        int[] Arr;       
        Arr=nhapRandom();
        xuatArr(Arr);
        System.out.println("Những Số Lẻ Tìm Được");        
        for(int i=0;i<Arr.length;i++){
            if(Arr[i] %2 !=0){
                
                System.out.println(Arr[i]);
               
            }        
        }
        return Arr;
    }
    // tìm số chẳn
    public static int[] timSoChan(){
        int[] Arr;      
        Arr=nhapRandom();
        xuatArr(Arr);
        System.out.println("Những Số Chẳn Tìm Được");        
        for(int i=0;i<Arr.length;i++){
            if(Arr[i] %2 ==0){                
                System.out.println(Arr[i]);               
            }        
        }
        return Arr;
    }
    
    // tính tổng 2 số chia hết cho 5 và 2
    public static int tongSoChia5v2(){
        
        int[] Arr;      
        Arr=nhapRandom();
        int tongSo=0;
        xuatArr(Arr);
              
        for(int i=0;i<Arr.length;i++){
            if(Arr[i] %2 ==0 && Arr[i]%5 ==0){                
                tongSo+=Arr[i];
                System.out.print("Tổng Của số chia hết cho 2 và 5: "+ tongSo);
                break;  
                              
            }
        } 
        if(tongSo==0){           
            System.out.println("Không Có Số Nào Trong mảng Chia Hết Cho 2 và 5!!! ");
            System.out.println("");               
               
        }
                
    return tongSo;
    }
    // giai thừa
    public static int giaiThua(){
        System.out.println("Nhập n giai thừa: ");
        int n_gThua= 1;
        int n=nhapSoNguyenDuong();
        if(n==1){
            return 1;
        }else{
            for(int i=1;i<=n;i++){
                n_gThua*=i;
                
            }
            System.out.println(n_gThua);
        }
        
        
        return n_gThua;
    }
    public static int soLeCuoiCung(){
        int[] Arr=nhapRandom();
        xuatArr(Arr);
        for (int i = Arr.length-1; i >=0; i--) {
            if(Arr[i] %2 !=0){                
                
                System.out.println("Số lẻ trong mảng: "+Arr[i]);
                break;
               
            }else{
                System.out.println("Không có Số lẻ trong mảng: ");
            }         
                    
        }
      
        return Arr.length-1;
    }

    public static int soChanCuoiCung(){
        int[] Arr=nhapRandom();
        xuatArr(Arr);
        for (int i = Arr.length-1; i < Arr.length; i--) {
            if(Arr[i] %2 ==0){                
                
                System.out.println("Số Chẳn trong mảng: "+Arr[i]);
                break;
               
            }else{
                System.out.println("Không có Số Chẳn trong mảng: ");
            }         
                    
        }
      
        return Arr.length-1;
    }

	
}