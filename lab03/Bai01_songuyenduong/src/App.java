import java.util.Scanner;

public class App {
    public static Scanner sc  = new Scanner(System.in);
    
    public static void main(final String[] args) throws Exception {  
        int sttMenu=0;
        int soMenu=11;      
        xuatMenu();
        do {
            sttMenu=chonMenu(sc, soMenu);
            xulyMenu(sttMenu);
        } while (true);
        
    }
    public static void xuatMenu(){
        System.out.println("========================>MENU<========================");
        System.out.println("1. Nhập Số Nguyên Dương");
        System.out.println("2. Tạo Mảng Và Chèn Phần Tử");
        System.out.println("3. Tìm Min Trong Mảng");
        System.out.println("4. Tìm Max Trong Mảng");
        System.out.println("5. Tìm Min từng xuất hiện Trong Mảng");
        System.out.println("6. Tìm Max từng xuất hiện Trong Mảng");
        System.out.println("7. Tính Tổng Các Số Chẳn Trong Mảng");
        System.out.println("8. Tính Tổng Các Số Lẻ Trong Mảng");
        System.out.println("9. Tính Tổng Các Số Lẻ Trong Mảng");
        System.out.println("10. Tính Tổng Các Số Chẳn Trong Mảng");
        System.out.println("11. Tính Tổng Các Số Chia Hết Cho 5 và 2 Trong Mảng");
        System.out.println("12. Tính n giai thừa");
        System.out.println("13. Tìm số lẻ cuối cùng");
        System.out.println("14. Tìm số chẳn cuối cùng");
        System.out.println("0. Thoát");
        System.out.println("======================================================");
    }

    public static int chonMenu(Scanner sc,int soMenu){
        soMenu=0;
        int chonMenu=0;
        do {
            System.out.println("Chọn 1 Chức Năng Tương ứng : ");
            chonMenu=HamTinhToan.nhapSoNguyenDuong();

        } while (0<=chonMenu && chonMenu<=soMenu);
        return chonMenu;
    }

    public static void xulyMenu(int stt_Menu){
        switch (stt_Menu) {
            case 1:
                System.out.println("1. Nhập Số Nguyên Dương");               
                HamTinhToan.nhapSoNguyenDuong();           
                break;
            case 2:
                System.out.println("2. Tạo Mảng Và Chèn Phần Tử");
                int n=HamTinhToan.kichThuocArr();
                int[] kt= HamTinhToan.inputPTArr(n);               
                HamTinhToan.xuatArr(kt);
                break;
            case 3:
                System.out.println("3. Tìm Min Trong Mảng");            
                HamTinhToan.minArr();
                break;
            case 4:
                System.out.println("4. Tìm Max Trong Mảng");            
                HamTinhToan.maxArr();
                break;
            case 5:
                System.out.println("5. Tìm Min từng xuất hiện Trong Mảng");
                HamTinhToan.allMinArr();          
                
                break;
            case 6:
                System.out.println("6. Tìm Max từng xuất hiện Trong Mảng");
                HamTinhToan.allMaxArr();
                break;
            case 7:
                System.out.println("7. Tính Tổng Các Số Chẳn Trong Mảng");
                HamTinhToan.tinhChanArr();
                break;
            case 8:
                System.out.println("8. Tính Tổng Các Số Lẻ Trong Mảng");
                HamTinhToan.tinhLeArr();
                break;
            case 9:
                System.out.println("9. Tìm Các Số Lẻ Trong Mảng");
                HamTinhToan.timSoLe();
                break;
            case 10:
                System.out.println("10. Tìm Các Số Chẳn Trong Mảng");
                HamTinhToan.timSoChan();
                break;
            case 11:
                System.out.println("11. Tính Tổng Các Số Chia Hết Cho 5 và 2 Trong Mảng");
                HamTinhToan.tongSoChia5v2();
                break;
            case 12:
                System.out.println("12. Tính n giai thừa");
                HamTinhToan.giaiThua();
                break;
            case 13:
                System.out.println("13. Tìm số lẻ cuối cùng");
                HamTinhToan.soLeCuoiCung();
                break;
            case 14:
                System.out.println("14. Tìm số chẳn cuối cùng");
                HamTinhToan.soChanCuoiCung();
                break;
            case 0:
                System.out.println("Bạn chọn chức năng thoát! Tạm biệt!");
                System.exit(0); // thoát chương trình
                break;
        }
    } 
}
